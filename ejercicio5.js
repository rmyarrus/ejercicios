/* PIEDRA PAPEL O TIJERA
Piedra, papel o tijera es un clásico juego de dos jugadores. Cada jugador elige piedra, papel o tijera. Los elementos se comparan y el jugador que elige el elemento más poderoso gana.

Los posibles resultados son:

Roca destruye tijeras.
Tijeras cortan papel.
El papel cubre la roca.
Si hay un empate, entonces el juego termina en empate.
Nuestro código dividirá el juego en cuatro partes:

Obtener la elección del usuario.
Obtenga la elección de la computadora.
Compare las dos opciones y determine un ganador.
Inicie el programa y muestre los resultados.*/


const eleccionUsuario = (entradaUsuario) => { //variable de entrada 

 entradaUsuario = entradaUsuario.toLowerCase();

  if (entradaUsuario === "tijeras" || entradaUsuario === "papel" || entradaUsuario === "piedra"){

  return entradaUsuario; 

  } else {

      return("Usted no ha elegido ninguna opción")

  }



}




const eleccionComputadora = () => {

  const numeroRandom = (Math.floor(Math.random() *3));

  switch (numeroRandom) {

    case 0:
      return "tijeras";
    case 1:
      return "papel";
    case 2:
      return "piedra"; 
      
    }

};

const determinarGanador = (eleccionComputadora, eleccionUsuario) => {

  if (eleccionUsuario == eleccionComputadora){

    return "El juego ha terminado en empate"

  } 

  if (eleccionUsuario === "piedra") {
    if (eleccionComputadora === "papel"){

      return "Lo lamento, la computadora ha ganado"

    }else {

      return "Felicitaciones, has ganado"

    }


  }

  if (eleccionUsuario === "papel"){
    if (eleccionComputadora === "tijeras"){

      return "Lo lamento, la computadora ha ganado"
    } else {

      return "Felicitaciones, has ganado"

    }



  }

  if (eleccionUsuario === "tijeras"){
    if(eleccionComputadora === "piedra"){

      return "Lo lamento, la computador ha ganado"

    } else { 

      return "Felicitaciones, has ganado"
    }

  }

};



const empezarJuego = () => {

  let getEleccionUsuario = eleccionUsuario("papel");
  let getEleccionComputadora = eleccionComputadora();
  document.write(`Usted ha elegido:  ${getEleccionUsuario}` + "<br>");
  document.write(`La computadora ha elegido:  ${getEleccionComputadora}` + "<br>");


  document.write(determinarGanador(getEleccionComputadora, getEleccionUsuario));


};

empezarJuego();
/*¡La carrera anual de Codecademy está a la vuelta de la esquina! Este año tenemos muchos participantes. 
Lo han contratado para escribir un programa que inscribirá a los corredores para la carrera y les dará instrucciones el día de la carrera.

Como línea de tiempo, el registro se vería así: 

Así es como funciona nuestro registro. Hay corredores adultos (mayores de 18 años) y corredores jóvenes (menores de 18 años). Pueden registrarse temprano o tarde. A los corredores se les asigna un número de carrera y una hora de inicio en función de su edad e inscripción.

Número de carrera:

Los primeros adultos reciben un número de carrera igual o superior a 1000.
Todos los demás reciben un número por debajo de 1000.
Hora de inicio:

Los inscritos adultos corren a las 9:30 a. m. o a las 11:00 a. m.
Los adultos tempranos corren a las 9:30 am.
Adultos tarde corren a las 11:00 am.
Los jóvenes que se inscriban corren a las 12:30 horas (independientemente de la inscripción).
¡Pero no planeamos corredores que tengan exactamente 18 años! Nos encargaremos de eso al final del proyecto.

Si se queda atascado durante este proyecto o le gustaría ver a un desarrollador experimentado trabajar en él, haga clic en " Desatascarse " para ver un video de guía del proyecto .

*/



corredor = () => {

    let numeroCarrera = Math.floor(Math.random() * 1000);

    let horarioRegistro = true;
    
    let corredor = prompt("¿Que edad tiene?")
if (corredor >= 18 && horarioRegistro) {

    alert(`Usted es un adulto y se registró temprano, su numero de corredor es ${numeroCarrera} y correrá a las 9:30`);

}

horarioRegistro = false;
let corredor2 = prompt("¿Que edad tiene corredor 2?")
if (corredor2 > 18 || horarioRegistro){

    alert(`Usted es un adulto y se registró de manera tardía, su numero de corredor es ${numeroCarrera} y correrá a las 11:00`);

}

let corredor3 = prompt("¿Que edad tiene corredor 3?")
if (corredor3 < 18){

    alert(`Usted es menor, su numero de corredor es ${numeroCarrera} y correrá a las 12:30`);


}else if (corredor3 <= 18){

    alert("Usted tiene 18 años, no puede competir")
}

}

corredor();
/* En lo profundo de su laboratorio meteorológico en la ladera de la montaña, el científico loco Kelvin ha dominado la predicción del tiempo.

Recientemente, Kelvin comenzó a publicar sus pronósticos del tiempo en su sitio web. Sin embargo, hay un problema: todos sus pronósticos describen la temperatura en Kelvin .

Con nuestro conocimiento de JavaScript, convirtamos Kelvin a Celsius, luego a Fahrenheit. */


//El valor permanece constante, ya que no se modifica.

const kelvin = 293;

// Restamos 273 a Celsius, sería igual a 23, despues convertimos Celsius en escala de Newton utilizando el metodo ".floor"
let Celsius = Math.floor(23 * (33 / 100));



// Utilizamos el metodo ".floor" para redondear el número decimal
let Fahrenheit = Math.floor(Celsius * (9 / 5) + 32);

document.write("Celsius en la escala de Newton es de ", Celsius.floor());

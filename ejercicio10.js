/* Días de entrenamiento
Como atleta experimentado, una de tus actividades favoritas es correr maratones. Utilizas un servicio llamado Training Days que te envía un mensaje para el evento en el que te inscribiste 
y los días que te quedan para entrenar.

Dado que también codifica, Training Days le ha pedido que los ayude a resolver un problema: el programa actualmente usa el alcance incorrecto para sus variables. 
Saben que esto puede ser problemático a medida que evoluciona su servicio. En este proyecto, hará que los días de capacitación sean más fáciles de mantener y menos propensos a
 errores mediante la corrección de alcances variables.

EJERCICIO CODECADEMY

 .*/
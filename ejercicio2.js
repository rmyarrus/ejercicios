/* Años del perro
Los perros maduran a un ritmo más rápido que los seres humanos. A menudo decimos que la edad de un perro se puede calcular en "años de perro" para tener en cuenta su crecimiento en comparación con un humano de la misma edad. En cierto modo, podríamos decir que el tiempo pasa rápido para los perros: 8 años en la vida de un ser humano equivalen a 45 años en la vida de un perro. ¿Qué edad tendrías si fueras un perro?

Así es como convierte su edad de "años humanos" a "años de perro":

Los dos primeros años de vida de un perro cuentan como 10,5 años de perro cada uno.
Cada año siguiente equivale a 4 años de perro.
Antes de que empieces a hacer los cálculos mentalmente, ¡deja que una computadora se encargue de ello! Con su conocimiento de operadores matemáticos y variables, use JavaScript para convertir su edad humana en años de perro.

Si se queda atascado durante este proyecto o le gustaría ver a un desarrollador experimentado trabajar en él, haga clic en " Desatascarse " para ver un video de guía del proyecto .*/


let miEdad = 24;
miEdad -= 2;

let añosDespues = miEdad;
añosDespues *= 4;



// Cuando reasignamos el valor de let, no hace faltar volver a poner let.

//Años caninos
let primerosAños = 2;
primerosAños *= 10,5;

let miEdadCanina = primerosAños + añosDespues;

let miNombre = "RICARDO";
miNombre = miNombre.toLowerCase()

document.write("Mi nombre es ", miNombre, ". ", "Tengo la edad humana de ", miEdad, " que en años caninos equivaldría a ", miEdadCanina);